;; This file is part of Anonymous.

;; Anonymous is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.

;; Anonymous is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.

;; You should have received a copy of the GNU General Public License
;; along with Anonymous.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (chickadee)
             (chickadee math vector)
             (chickadee render sprite)
             (chickadee render texture))

(define sprite #f)

(define (load)
  (set! sprite (load-image "static/image/anonymous.png")))

(define (draw alpha)
  (draw-sprite sprite (vec2 256.0 176.0)))

(add-hook! load-hook load)
(add-hook! draw-hook draw)

(run-game)
